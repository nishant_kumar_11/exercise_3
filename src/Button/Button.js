import React from 'react';
import './button.css';

export function Button(props) {
    const {buttonname, myClass} = props;
    return (
        <>
            <div className={`button ${myClass}`}>{buttonname}</div>
        </>
    );
}
