import React from 'react';
import { Button } from '../Button/Button'

export function BlockTwo() {

    return (
        <>
            <div className="two-one">
                <Button myClass="primary-cta-full" buttonname="Primary CTA" />
            </div>
        </>
    );
}
