import React from 'react';
import { Button } from '../Button/Button'

export function BlockOne() {

    return (
        <>
            <div className="one">
                <Button myClass="primary-cta" buttonname="Primary CTA" />
                <Button myClass="secondary-cta" buttonname="Secondary CTA (s)" />
                <Button myClass="secondary-cta-d" buttonname="Tertiary CTA (D)" />
            </div>
            <div className="two">
                <Button myClass="primary-cta" buttonname="Primary CTA" />
                <Button myClass="secondary-cta" buttonname="Secondary CTA (s)" />
                <Button myClass="secondary-cta-l" buttonname="Tertiary CTA (L)" />
            </div>
        </>
    );
}
