import './App.css';
import { BlockOne } from './BlockOne/BlockOne'
import { BlockTwo } from './BlockTwo/BlockTwo'

function App() {
  return (
    <div className="App">
      <div className="block-one">
        <BlockOne />
      </div>
      <div className="block-two">
        <BlockTwo />
      </div>
    </div>
  );
}

export default App;
